def parseLastChangeLog(){
    //get last file change log
    bat "git log --name-only -1 > commandResult"
    def filenames = readFile('commandResult')
    def currentfile = filenames.readLines()
    def found_h, found_cpp
    found_cpp = currentfile.find{element -> element =~ /.+\.cpp/}
    found_h = currentfile.find{element -> element =~ /.+\.h/}
    if(found_h || found_cpp){
        _SRC_CHANGED = true
    }
    //debug node
    echo _SRC_CHANGED
}

def getLastUserInfo(){
    bat "git log --pretty=format:%%ce -1 > commandResult"
    _LAST_SUBMITTER_EMAIL = readFile('commandResult').trim()
    //debug node
    echo _LAST_SUBMITTER_EMAIL
}
//定义并checkout对应branch
def defAndCheckoutBranch(def _branch, def _platform){
    switch(_platform){
        case "Win":
        bat "git checkout -b ${_branch} || echo ${_branch} already exist"
        bat "git checkout ${_branch} || echo already in ${_branch} branch"
        break
        case "Mac":
        sh "git checkout -b ${_branch} || echo ${_branch} already exist"
        sh "git checkout ${_branch} || echo already in ${_branch} branch"
        break
        default:
        break
    }
}

def checkOut(def _url, def _branch, def _platform, def _task){
    echo """
    ###############################################################################
    ### Check out on ${_platform} start ###########################################
    ###############################################################################
    """
    checkout([
    $class: 'GitSCM', branches: [[name: "${_branch}"]], extensions: [[$class:'CheckoutOption',timeout:200],[$class:'CloneOption',depth:0,noTags:false,reference:'',shallow:false,timeout:200]], 
    userRemoteConfigs: [[credentialsId: 'atg-bitbucket-cicd', url: "${_url}"]]
    ])

    defAndCheckoutBranch("${_branch}", "${_platform}")

    echo """
    ###############################################################################
    ### Check out on ${_platform} end #############################################
    ###############################################################################
    """
    if(_AUTO_TRIGGERED){
        getLastUserInfo()
    }
    parseLastChangeLog()
}

return [
    checkOut: this.&checkOut
]