def initEnvParams(def _version, def _branch, def _platform = "Win", def _solutionConfig = "Development Editor"){
    _UE_VERSION = _version
    _BRANCH = _branch
    _PLATFORM = _platform
    _SOLUTION_CONFIG = _solutionConfig
    if(_platform == "Win" || "Android"){
        _AGENT_LABEL = "Windows_server"
    }
    else if(_platform == "Mac" || "Ios"){
        _AGENT_LABEL = "Mac_server"
    }
}

def startAndEndStage(def _keyword){
    switch(_keyword){
        case "start":
        echo """
        ###############################################################################
        ### Starting Stage :${env.STAGE_NAME} #########################################
        ###############################################################################
        """
        break
        case "end":
        echo """
        ###############################################################################
        ### Ending Stage :${env.STAGE_NAME} ###########################################
        ###############################################################################
        """
        break
        default:
        break
    }

}

def preparePath(def _version, def _platform) {
    def workPath
    //debug
    _UE_VERSION = _version
    echo _version
    switch (_version){
        case "4.26":
            workPath = "UE4\\4.26"
            break
        case "4.27":
            workPath = "UE4\\4.27"
            break
        case "5.0":
            workPath = "UE5\\5.0ea"
            break
        default:
            workPath = "other version"
    }
    if(!fileExists(workPath)) {
        switch (_platform) {
            case "Win":
                bat "md ${workPath}"
                break
            case "MAC":
                sh "mkdir ${workPath}"
                break
            default:
            println("Aborting: Unknown Platform")
        }
    }
    _WORKSPACEDIR = ".\\${workPath}"
}

def prepareEnv(def _bit_config_json, def _version, def _platform){
    try{
        _CHECKOUT_RETRY_COUNT = _bit_config_json["checkout-retry-time"].toInteger()
        assert(_CHECKOUT_RETRY_COUNT>0)
    }
    catch(Exception ex){
        println("checkout-retry-time is set to be no-meaningful, using default value.")
    }
    preparePath(_version, _platform)
}

return [
    prepareEnv: this.&prepareEnv,
    startAndEndStage: this.&startAndEndStage,
    initEnvParams: this.&initEnvParams,
]