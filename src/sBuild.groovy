def windowsBuild(def _solutionConfig){
    timeout(unit: 'HOURS', time: 2){
        bat "call GenerateProjectFiles.bat"
        switch(_solutionConfig){
            case "Development Editor":
            bat """
            Engine\\Build\\BatchFiles\\Build.bat -Target="UE4Editor Win64 Development" -Target="ShaderCompileWorker Win64 Development -Quiet" -WaitMutex -FromMsBuild
            """
            break
            case "Development Client":
            bat """
            Engine\\Build\\BatchFiles\\Build.bat UE4Client Win64 Development -WaitMutex -FromMsBuild 
            """
            break
            default:
            break
        }
    }
}

def macBuild(def _solutionConfig){
    timeout(unit: 'HOURS', time: 2){
        //sh "call GenerateProjectFiles.bat"
    }
}

def androidBuild(def _solutionConfig){
    timeout(unit:'HOURS', time:2){

    }
}

def iosBuild(def _solutionConfig){
    timeout(unit:'HOURS', time:2){

    }
}

def build(def _platform, def _solutionConfig){
    switch (_platform){
        case "Win":
        windowsBuild(_solutionConfig)
        break

        case "Mac":
        macBuild(_solutionConfig)
        break

        case "Android":
        androidBuild(_solutionConfig)
        break
        
        case "Ios":
        iosBuild(_solutionConfig)
        break

        default:
        break
    }
}

return [
    build: this.&build,
]